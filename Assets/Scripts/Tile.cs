﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    private GameManager m_manager;
    public GameManager.Symbols Symbol { get; private set; }
    private Transform m_child;
    private Collider m_collider;


    //analytics variables

    // get reference to child object of this object
    // get reference to box collider on this object
    private void Start()
    {
        m_child = this.transform.GetChild( 0 );
        m_collider = GetComponent<Collider>();
    }

    // when there is a click on this object,
    //activate a function in gameManager named tileSelected passing this gameobject
    private void OnMouseDown()
    {
        m_manager.TileSelected( this );


    }

    //this initaializes the variables m_manager and Symbol
    //it also takes in the parameters GameManager and GameManager.Symbols
    public void Initialize( GameManager manager, GameManager.Symbols symbol )
    {
        m_manager = manager;
        Symbol = symbol;
    }

    //this tells the card to reveal itself
    // stops any coroutines running 
    //starts new corountine passing parameters that effect the target rotation and time
    // ** flips them face up ** 
    public void Reveal()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 180, 0.8f ) );
        m_collider.enabled = false;
    }

    // this tells the card to hide itself
    // stops any coroutines playing
    // starts new coroutine passing parameters that effect ther target rot and time
    // ** flips them face down ** 
    public void Hide()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 0, 0.8f ) );
        m_collider.enabled = true;
    }

    //this coroutine is responsible for rotating the tile
    //depending on the parameters passed, it wil either make the card face up or down
    private IEnumerator Spin( float target, float time )
    {
        float timer = 0;
        float startingRotation = m_child.eulerAngles.y;
        Vector3 euler = m_child.eulerAngles;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            euler.y = Mathf.LerpAngle( startingRotation, target, timer / time );
            m_child.eulerAngles = euler;
            yield return null;
        }
        euler.y = target;
        m_child.eulerAngles = euler;
    }
}
