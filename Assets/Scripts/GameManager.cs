﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;

    // analytics variables
    int matchFails;
    static int levelNum = 1;
    float levelTimer;

    int currentStreak;
    int maxStreak;

    float clickTimer;
    static int levelsCompleted;


    [System.Flags]
    public enum Symbols
    {
        Waves =             1 << 0,
        Dot =               1 << 1,
        Square =            1 << 2,
        LargeDiamond =      1 << 3,
        SmallDiamonds =     1 << 4,
        Command =           1 << 5,
        Bomb =              1 << 6,
        Sun =               1 << 7,
        Bones =             1 << 8,
        Drop =              1 << 9,
        Face =              1 << 10,
        Hand =              1 << 11,
        Flag =              1 << 12,
        Disk =              1 << 13,
        Candle =            1 << 14,
        Wheel =             1 << 15
    }

    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;

    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;

    private void Start()
    {
        LoadLevel( CurrentLevelNumber );
    }

    void Update()
    {
        levelTimer += Time.deltaTime;
        clickTimer += Time.deltaTime;
        
    }

    //this help load the level when start of the game occurs
    private void LoadLevel( int levelNumber )
    {
        LevelDescription level = Levels[levelNumber % Levels.Length];

        List<Symbols> symbols = GetRequiredSymbols( level );

        //this sets all the tiles in place depending on the values given
        // this also takes into account what level it is 
        for ( int rowIndex = 0; rowIndex < level.Rows; ++rowIndex )
        {
            float yPosition = rowIndex * ( 1 + TileSpacing );
            for ( int colIndex = 0; colIndex < level.Columns; ++colIndex )
            {
                float xPosition = colIndex * ( 1 + TileSpacing );
                GameObject tileObject = Instantiate( TilePrefab, new Vector3( xPosition, yPosition, 0 ), Quaternion.identity, this.transform );
                int symbolIndex = UnityEngine.Random.Range( 0, symbols.Count );
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol( symbols[symbolIndex] );
                tileObject.GetComponent<Tile>().Initialize( this, symbols[symbolIndex] );
                symbols.RemoveAt( symbolIndex );
            }
        }

        SetupCamera( level );
    }

    // creates a new list of symbols based on selected symbols
    // set the total number of cards based upon the number of cards in a row and column
    private List<Symbols> GetRequiredSymbols( LevelDescription level )
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //create an array of all the symbols
        {
            Array allSymbols = Enum.GetValues( typeof( Symbols ) );

            m_cardsRemaining = cardTotal;

            // this helps to see if their are and odd number of cards
            // throws a message if true 
            if ( cardTotal % 2 > 0 )
            {
                new ArgumentException( "There must be an even number of cards" );
            }


            foreach ( Symbols symbol in allSymbols )
            {
                if ( ( level.UsedSymbols & symbol ) > 0 )
                {
                    symbols.Add( symbol );
                }
            }
        }

        // makes sure that their are symbols to assign to cards
        // or makes sure their is enough cards for the assigned symbols
        {
            if ( symbols.Count == 0 )
            {
                new ArgumentException( "The level has no symbols set" );
            }
            if ( symbols.Count > cardTotal / 2 )
            {
                new ArgumentException( "There are too many symbols for the number of cards." );
            }
        }

        //determines if there are any symbols that need to repeat
        //does so if the number of potential mataches is greater than the smount of symbols used
        {
            int repeatCount = ( cardTotal / 2 ) - symbols.Count;
            if ( repeatCount > 0 )
            {
                List<Symbols> symbolsCopy = new List<Symbols>( symbols );
                List<Symbols> duplicateSymbols = new List<Symbols>();
                for ( int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex )
                {
                    int randomIndex = UnityEngine.Random.Range( 0, symbolsCopy.Count );
                    duplicateSymbols.Add( symbolsCopy[randomIndex] );
                    symbolsCopy.RemoveAt( randomIndex );
                    if ( symbolsCopy.Count == 0 )
                    {
                        symbolsCopy.AddRange( symbols );
                    }
                }
                symbols.AddRange( duplicateSymbols );
            }
        }

        symbols.AddRange( symbols );

        return symbols;
    }

    //this helps set the positon of one card depending on the previous card
    private Vector2 GetOffsetFromSymbol( Symbols symbol )
    {
        Array symbols = Enum.GetValues( typeof( Symbols ) );
        for ( int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex )
        {
            if ( ( Symbols )symbols.GetValue( symbolIndex ) == symbol )
            {
                return new Vector2( symbolIndex % 4, symbolIndex / 4 ) / 4f;
            }
        }
        Debug.Log( "Failed to find symbol" );
        return Vector2.zero;
    }

    // this sets the camera up to expand with the number of rows
    // makes sure the camera is centered according to how many row and columns there are
    private void SetupCamera( LevelDescription level )
    {
        Camera.main.orthographicSize = ( level.Rows + ( level.Rows + 1 ) * TileSpacing ) / 2;
        Camera.main.transform.position = new Vector3( ( level.Columns * ( 1 + TileSpacing ) ) / 2, ( level.Rows * ( 1 + TileSpacing ) ) / 2, -10 );
    }

    //this helps decipher if a match has been made
    // stores the two cards info and check to see if their symbols match
    // if they do then tell the waitforhide that they match
    //else tell waitforhide they dont
    public void TileSelected( Tile tile )
    {
        Analytics.CustomEvent("time between clicks", new Dictionary<string, object>
        {
            {"time betweeen clicks", clickTimer }

        });

        clickTimer = 0;
        if ( m_tileOne == null )
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        else if ( m_tileTwo == null )
        {
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            if ( m_tileOne.Symbol == m_tileTwo.Symbol )
            {
                StartCoroutine( WaitForHide( true, 1f ) );
            }
            else
            {
                StartCoroutine( WaitForHide( false, 1f ) );
            }
        }
    }

    // this helps move the game through levels
    // if all the levels have been cycled through, then send a game over message
    // otherwise load the next level
    private void LevelComplete()
    {
        ++CurrentLevelNumber;
        if ( CurrentLevelNumber > Levels.Length - 1 )
        {
            Debug.Log( "GameOver" );

        }
        else
        {
            levelsCompleted++;
            SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
        }
    }

    void OnApplicationQuit()
    {
        Debug.Log("game ended");

        Analytics.CustomEvent("levels completed per game", new Dictionary<string, object> {
            {"levels completed in the game", levelsCompleted}

        });
    }

    // this helps decipher between a match and no match
    // if there is a match destory the selected card and reduce the # of cards by 2
    //otherwise rotate both cards back and empty the holders
    // if no cards remain, execute levelcomplete
    private IEnumerator WaitForHide( bool match, float time )
    {
        float timer = 0;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            if ( timer >= time )
            {
                break;
            }
            yield return null;
        }
        if ( match )
        {
            currentStreak++;

            if (currentStreak > maxStreak) {
                maxStreak = currentStreak;
            }

            Destroy( m_tileOne.gameObject );
            Destroy( m_tileTwo.gameObject );
            m_cardsRemaining -= 2;
        }
        else
        {
            matchFails++;

            m_tileOne.Hide();
            m_tileTwo.Hide();
        }
        m_tileOne = null;
        m_tileTwo = null;

        if ( m_cardsRemaining == 0 )
        {
            // calculate how many failed matches were made per level
            Analytics.CustomEvent("Failed matchs", new Dictionary<string, object>
            {
                { "failed Matchs in: " + levelNum , matchFails}
            });

            Analytics.CustomEvent("Match Time", new Dictionary<string, object>
            {
                {"Match time in level: " + levelNum, levelTimer}
            });

            Analytics.CustomEvent("Max Streak", new Dictionary<string, object>
            {
                {"Max streak in level: " + levelNum, maxStreak}
            });

            levelNum++;
            LevelComplete();

        }
    }
}
